<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use App\Models\Products;

class ProductController extends Controller
{
    public function index()
    {
        $products = Products::all();

        return response()->json([
            'data' => ProductResource::collection($products)
        ]);
    }

    public function show(int $id)
    {
        $product = Products::find($id);

        if (empty($product)) {
            return response()->json(["message" => "Product not found"], 404);
        }

        return response()->json([
            'data' => new ProductResource($product)
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'slug' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        $product = new Products();
        $product->slug = $request->slug;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->image = $request->image;
        $product->save();

        return response()->json([
            'data' => new ProductResource($product),
            "message" => "Product added"
        ], 201);
    }

    public function showCategories(int $id)
    {
        $product = Products::find($id);

        if (empty($product)) {
            return response()->json(["message" => "Product not found"], 404);
        }

        return response()->json([
            'data' => CategoryResource::collection($product->categories)
        ]);
    }

    public function update(Request $request, int $id)
    {
        $product = Products::find($id);

        if (empty($product)) {
            return response()->json(["message" => "Product not found"], 404);
        }

        $product->slug = $request->slug ?? $product->slug;
        $product->title = $request->title ?? $product->title;
        $product->description = $request->description ?? $product->description;
        $product->image = $request->image ?? $product->image;

        $product->save();

        return response()->json([
            'data' => new ProductResource($product),
            "message" => "Product updated"
        ], 200);
    }

    public function delete(int $id)
    {

        $product = Products::find($id);

        if (empty($product)) {
            return response()->json(["message" => "Product not found"], 404);
        }

        $product->delete();

        return response()->json(["message" => "Product deleted"], 204);
    }
}
