<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Models\Categories;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Categories::all();

        return response()->json([
            'data' => CategoryResource::collection($categories)
        ]);
    }


    public function show(int $id)
    {
        $category = Categories::find($id);

        if (empty($category)) {
            return response()->json(["message" => "Product not found"], 404);
        }

        return response()->json([
            'data' => new CategoryResource($category)
        ]);
    }


    public function store(Request $request)
    {
        $category = new Categories();

        $category->title = $request->title;
        $category->description = $request->description;
        $category->save();

        return response()->json([
            'data' => new CategoryResource($category),
            "message" => "Product added"
        ], 201);
    }


    public function update(Request $request, int $id)
    {

        $category = Categories::find($id);

        if (empty($category)) {
            return response()->json(["message" => "Product not found"], 404);
        }

        $category->title = $request->title ?? $category->title;
        $category->description = $request->description ?? $category->description;

        $category->save();

        return response()->json([
            'data' => new CategoryResource($category),
            "message" => "Product updated"
        ], 201);

    }

    public function delete(int $id)
    {

        $category = Categories::find($id);

        if (empty($category)) {
            return response()->json(["message" => "Product not found"], 404);
        }

        $category->delete();

        return response()->json(["message" => "Product deleted"], 200);
    }
}
