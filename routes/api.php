<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', [\App\Http\Controllers\ProductController::class, 'index']);
Route::get('/products/{id}', [\App\Http\Controllers\ProductController::class, 'show']);
Route::get('/products/{id}/categories', [\App\Http\Controllers\ProductController::class, 'showCategories']);
Route::post('/products', [\App\Http\Controllers\ProductController::class, 'store']);
Route::patch('/products/{id}', [\App\Http\Controllers\ProductController::class, 'update']);
Route::delete('/products/{id}', [\App\Http\Controllers\ProductController::class, 'delete']);

Route::get('/categories', [\App\Http\Controllers\CategoryController::class, 'index']);
Route::get('/categories/{id}', [\App\Http\Controllers\CategoryController::class, 'show']);
Route::post('/categories', [\App\Http\Controllers\CategoryController::class, 'store']);
Route::patch('/categories/{id}', [\App\Http\Controllers\CategoryController::class, 'update']);
Route::delete('/categories/{id}', [\App\Http\Controllers\CategoryController::class, 'delete']);

