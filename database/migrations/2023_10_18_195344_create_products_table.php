<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->text('slug');
            $table->text('title');
            $table->text('description');
            $table->text('image')->nullable();
            $table->timestamps();
        });

        DB::table('products')->insert([
            [
                'id' => 1,
                'slug' => 'catan',
                'title' => 'Catan',
                'description' => 'Basis Catan, een klassieker',
                'image' => '/data/catan.png'
            ],
            [
                'id' => 2,
                'slug' => 'catan-steden-ridders',
                'title' => 'Catan Steden en Ridders',
                'description' => 'Meer Catan, nu met steden en ridders',
                'image' => '/data/steden_ridders.png'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
